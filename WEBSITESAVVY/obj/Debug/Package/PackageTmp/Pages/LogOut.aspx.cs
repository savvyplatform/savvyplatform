﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WEBSITESAVVY.Pages
{
    public partial class LogOut : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack == false)
            {
                Session.Abandon();
                Session.Clear();
                HttpCookie ck = new HttpCookie("MaGDV","");
                ck.Expires = DateTime.Now;
                Response.Cookies.Add(ck);
                Response.Redirect("~/Pages/Login.aspx");
            }
            
           
        }
    }
}