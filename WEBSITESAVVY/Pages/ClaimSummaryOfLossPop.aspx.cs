﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WEBSITESAVVY.DAO;
using System.Data;
using WEBSITESAVVY.DTO;

namespace WEBSITESAVVY.Pages
{
    public partial class ClaimSummaryOfLossPop : System.Web.UI.Page
    {
        static String claimID = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!IsPostBack)
            {
                HttpCookie ck = Request.Cookies["MaGDV"];
                if (ck != null)
                {
                    string id = Request.Cookies["MaGDV"].Value;


                    if (Session["ThamChieu"] != null)
                    {
                        claimID = Session["ThamChieu"].ToString();

                        grDanhMucThietHaiSR01.Visible = false;
                        grDanhMucThietHaiILA.Visible = false;

                        if (Request.QueryString["type"] != null)
                        {
                            String type = Request.QueryString["type"].ToString();
                            if (type == "SR01")
                            {
                                grDanhMucThietHaiSR01.Visible = true;
                            }
                            else if (type == "ILA")
                            {
                                grDanhMucThietHaiILA.Visible = true;
                            }
                        }


                        loadDanhMucThietHai();

                        load_Damaged();
                    }
                }
            }
        }

        private void loadDanhMucThietHai()
        {
            DanhMucThietHaiDAO danhMucThietHaiDAO = new DanhMucThietHaiDAO();
            DataTable dataSource = danhMucThietHaiDAO.DanhSachDanhMucThietHaiTheoClaim(claimID);

            grDanhMucThietHaiILA.DataSource = dataSource;
            grDanhMucThietHaiILA.DataBind();

            grDanhMucThietHaiSR01.DataSource = dataSource;
            grDanhMucThietHaiSR01.DataBind();
        }

        private void load_Damaged()
        {
            HangMucDAO dao = new HangMucDAO();
            drDamaged.DataSource = dao.DanhSachHangMuc();
            drDamaged.DataValueField = "ID_HangMuc";
            drDamaged.DataTextField = "TenHangMuc";
            drDamaged.DataBind();
        }

        protected void btnCreatnew_Click(object sender, EventArgs e)
        {
            panelGrid.Visible = false;
            panelAddEdit.Visible = true;

            btnAdd.Visible = true;
            btnUpdate.Visible = false;

            resetForm();
        }

        private void resetForm()
        {
            hiddenDanhMucID.Value = "";
            drDamaged.SelectedValue = "0";
            txtTotally.Text = "";
            txtPartial.Text = "";
            txtYKienGDV.Text = "";
            checkTinhTrang.Checked = false;
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            panelGrid.Visible = true;
            panelAddEdit.Visible = false;
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                lblMessage.Text = "";

                DanhMucThietHaiDAO dao = new DanhMucThietHaiDAO();

                DanhMucThietHaiDTO thietHai = new DanhMucThietHaiDTO();
                thietHai.MaClaim = Session["ThamChieu"].ToString();
                thietHai.MaHangMuc = int.Parse(drDamaged.SelectedValue);
                thietHai.ThietHaiHoanToan = txtTotally.Text;
                thietHai.TrangThai = txtPartial.Text;
                if(checkTinhTrang.Checked)
                    thietHai.TinhTrang = "Yes";
                else
                    thietHai.TinhTrang = "No";
                thietHai.YKienGDV = txtYKienGDV.Text;
                thietHai.MoTaChung = txtMoTaChung.Text;
                dao.ThemDanhMucThietHai(thietHai);

                Response.Write("<script>parent.reloaData();</script>");
                panelGrid.Visible = true;
                panelAddEdit.Visible = false;

                loadDanhMucThietHai();
            }catch(Exception ex){
                lblMessage.Text = ex.Message;
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                lblMessage.Text = "";

                DanhMucThietHaiDAO dao = new DanhMucThietHaiDAO();

                DanhMucThietHaiDTO thietHai = new DanhMucThietHaiDTO();
                thietHai.Ma = int.Parse( hiddenDanhMucID.Value );
                thietHai.MaClaim = Session["ThamChieu"].ToString();
                thietHai.MaHangMuc = int.Parse(drDamaged.SelectedValue);
                thietHai.ThietHaiHoanToan = txtTotally.Text;
                thietHai.TrangThai = txtPartial.Text;
                if (checkTinhTrang.Checked)
                    thietHai.TinhTrang = "Yes";
                else
                    thietHai.TinhTrang = "No";
                thietHai.YKienGDV = txtYKienGDV.Text;
                thietHai.MoTaChung = txtMoTaChung.Text;
                dao.CapNhatDanhMucThietHai(thietHai);

                panelGrid.Visible = true;
                panelAddEdit.Visible = false;

                Response.Write("<script>parent.reloaData();</script>");
                loadDanhMucThietHai();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }

        protected void grDanhMucThietHaiSR01_RowEditing(object sender, GridViewEditEventArgs e)
        {
			GridViewRow grRow = grDanhMucThietHaiSR01.Rows[e.NewEditIndex];
            string id = ((HiddenField)grRow.FindControl("hiddenID")).Value;
            e.Cancel = true;

            DanhMucThietHaiDAO dao = new DanhMucThietHaiDAO();
            DataRow dtRow = dao.GetDanhMucThietHai(id);

            panelGrid.Visible = false;
            panelAddEdit.Visible = true;
            btnAdd.Visible = false;
            btnUpdate.Visible = true;

            hiddenDanhMucID.Value = id;
            drDamaged.SelectedValue = dtRow["ID_HangMuc"].ToString();
            txtMoTaChung.Text = dtRow["TinhTrangChung"].ToString();
            txtTotally.Text = dtRow["ThietHaiHoanToan"].ToString();
            txtPartial.Text = dtRow["TrangThai"].ToString();
            txtYKienGDV.Text = dtRow["YKienGDV"].ToString();

            if(dtRow["TinhTrang"].ToString().ToLower() == "yes")
                checkTinhTrang.Checked = true;
            else
                checkTinhTrang.Checked = false;

        }

        protected void grDanhMucThietHaiILA_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GridViewRow grRow = grDanhMucThietHaiILA.Rows[e.NewEditIndex];
            string id = ((HiddenField)grRow.FindControl("hiddenID")).Value;
            e.Cancel = true;

            DanhMucThietHaiDAO dao = new DanhMucThietHaiDAO();
            DataRow dtRow = dao.GetDanhMucThietHai(id);

            panelGrid.Visible = false;
            panelAddEdit.Visible = true;
            btnAdd.Visible = false;
            btnUpdate.Visible = true;

            hiddenDanhMucID.Value = id;
            drDamaged.SelectedValue = dtRow["ID_HangMuc"].ToString();
            txtMoTaChung.Text = dtRow["TinhTrangChung"].ToString();
            txtTotally.Text = dtRow["ThietHaiHoanToan"].ToString();
            txtPartial.Text = dtRow["TrangThai"].ToString();
            txtYKienGDV.Text = dtRow["YKienGDV"].ToString();

            if (dtRow["TinhTrang"].ToString().ToLower() == "yes")
                checkTinhTrang.Checked = true;
            else
                checkTinhTrang.Checked = false;

        }

        protected void grDanhMucThietHaiILA_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            GridViewRow grRow = grDanhMucThietHaiILA.Rows[e.RowIndex];
            string id = ((HiddenField)grRow.FindControl("hiddenID")).Value;

            DanhMucThietHaiDAO dao = new DanhMucThietHaiDAO();
            dao.XoaDanhMucThietHai(int.Parse( id ));
            loadDanhMucThietHai();
            Response.Write("<script>parent.reloaData();</script>");
        }

        protected void grDanhMucThietHaiSR01_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            GridViewRow grRow = grDanhMucThietHaiSR01.Rows[e.RowIndex];
            string id = ((HiddenField)grRow.FindControl("hiddenID")).Value;

            DanhMucThietHaiDAO dao = new DanhMucThietHaiDAO();
            dao.XoaDanhMucThietHai(int.Parse(id));
            loadDanhMucThietHai();
            Response.Write("<script>parent.reloaData();</script>");
        }

    }
}